module Api
  class NotificationsController < ApplicationController
    before_action :set_notification, only: [:show, :update, :destroy]

    def index
      @notifications = Notification.all
      json_response(@notifications)
    end

    def create
      @notification = Notification.create!(notification_params)
      json_response(@notification, :created)
    end

    def show
      json_response(@notification)
    end

    def update
      @notification.update(notification_params)
      head :no_content
    end

    def destroy
      @notification.destroy
      head :no_content
    end

    private

    def notification_params
      params.permit(:messageId)
    end

    def set_notification
      @notification = Notification.find(params[:id])
    end

  end
end
